Programación para crear Máquinas del Tecno-Común
================================================

Este proyecto consiste en un libro para enseñar a programar 
en computadoras a activistas y militantes de movimientos sociales.

Entrega las herramientas necesarias y suficientes para poder crear
máquinas de combate contra el capital.

El libro está escrito utilizando el sistema Org-mode y considera diversos temas,
desde lo más básico, lógica, estructuras de control, seguridad y lenguajes de programación.

Espero que lo disfrutes y aprendas, compartas y colabores con este proyecto.
